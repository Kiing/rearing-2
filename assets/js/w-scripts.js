( function ( $ ) {

	// scripts here
	$( '.search-form' ).on( 'submit', function ( e ) {
		var search = $( this ).find( 'input' );
		if ( search.val().length < 1 ) {
			e.preventDefault();
			search.trigger( 'focus' );
		}
	} );

	$('.list-item').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		nextArrow: '.next',
     	prevArrow: '.btn-slick',
		 responsive: [{
			breakpoint: 768,
			settings: {
			  arrows: false,
			  centerPadding: '40px',
			  slidesToShow: 1
			}
		  },
		  {
			breakpoint: 480,
			settings: {
			  arrows: false,
			  centerPadding: '40px',
			  slidesToShow: 1
			}
		  }
		]	
	});

	$('.menu-mobile .icon').click(function(){
		$('.menu-mobile .list-menu').slideToggle('slow');
		$('.menu-mobile .list-menu').addClass('show');
		$('.overlay').addClass('show');
	});

	$('.overlay').click(function(){
		$(this).removeClass('show');
		$('.menu-mobile .list-menu').removeClass('show');
		$('.menu-mobile .list-menu').slideToggle('slow');
	});

}( jQuery ) );	